using MealPlanner.Managers.MealManager;
using MealPlanner.UI.Cli;

namespace MealPlanner
{
    public class Application
    {
        public static void Main()
        {
            var consoleService = new ConsoleService();
            var mealManager = new MealManager();
            var mealEditor = new MealEditor(consoleService, mealManager);
            var menu = new Menu(consoleService, mealEditor);
            menu.RunMenu();
        }
    }
}