using System;

namespace MealPlanner.UI.Cli
{
    public class ConsoleService : IConsoleService
    {
        public string ReadLine()
        {
            return Console.ReadLine().Trim();
        }

        public void WriteLines(string[] lines)
        {
            foreach (var line in lines)
                Console.WriteLine(line);
        }

        public void Clear()
        {
            Console.Clear();
        }
    }
}