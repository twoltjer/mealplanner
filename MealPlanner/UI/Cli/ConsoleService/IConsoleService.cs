namespace MealPlanner.UI.Cli
{
    public interface IConsoleService
    {
        public void WriteLines(string[] lines);
        public string ReadLine();

        public void Clear();
    }
}