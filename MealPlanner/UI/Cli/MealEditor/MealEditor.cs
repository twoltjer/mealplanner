﻿using MealPlanner.Definitions;
using MealPlanner.Managers;
using System;
using System.Linq;

namespace MealPlanner.UI.Cli
{
    /**
     * A CLI implementation of a meal editor. Uses a ConsoleService and a MealManager and allows 
     * the user to perform MealManager operations from the console. 
     */
    public class MealEditor : IMealEditor
    {
        private readonly IConsoleService _consoleService;
        private readonly IMealManager _mealManager;

        /**
         * Create a meal editor, using an IConsoleService and an IMealManager.
         * @param consoleService the service to interact with the user console
         * @param mealManager the meal manager that maintains meal data
         */
        public MealEditor(
            IConsoleService consoleService,
            IMealManager mealManager)
        {
            _consoleService = consoleService;
            _mealManager = mealManager;
        }

        /**
         * Run the meal editor for the user. May be exited with the "quit" command
         */
        public void RunMealEditor()
        {
            while(true)
            {
                _consoleService.WriteLines(new[]
                {
                    "",
                    "Welcome to the meal editor!",
                    "What would you like to do?",
                    "For options, type \"help\""
                });
                var input = _consoleService.ReadLine();
                switch (input.ToLower())
                {
                    case "help":
                        DisplayMealEditorHelp();
                        break;
                    case "list":
                    case "ls":
                        ListMeals();
                        break;
                    case "add":
                        AddMeal();
                        break;
                    case "edit":
                    case "ed":
                        EditMeal();
                        break;
                    case "del":
                    case "remove":
                    case "rm":
                        RemoveMeal();
                        break;
                    case "quit":
                    case "exit":
                    case "q":
                        return;
                    default:
                        DisplayUnkownCommandMessage(input);
                        break;
                }
            }
        }

        private void DisplayUnkownCommandMessage(string input)
        {
            _consoleService.WriteLines(new[]
            {
                $"Invalid command: \"{input}\". Please try again."
            });
        }

        private void RemoveMeal()
        {
            _consoleService.WriteLines(new[]
            {
                "Enter an index for a meal you wish to remove, or enter \"quit\" or \"exit\" to go back."
            });
            ListMeals();
            while (true)
            {
                var input = _consoleService.ReadLine();
                if (int.TryParse(input, out var index))
                {
                    if (_mealManager.IsValidMealIndex(index))
                    {
                        _mealManager.RemoveMeal(index);
                        return;
                    }
                    else
                    {
                        _consoleService.WriteLines(new[]
                        {
                            $"Invalid index \"{index}\". Please try again."
                        });
                    }
                }
                else
                {
                    switch(input)
                    {
                        case "quit":
                        case "exit":
                        case "q":
                            return;
                        default:
                            _consoleService.WriteLines(new[]
                            {
                                $"Invalid input \"{input}\". Please try again."
                            });
                            break;
                    }
                }
            }
        }

        private void EditMeal()
        {
            while (true)
            {
                _consoleService.WriteLines(new[]
                {
                    "What kind of edit do you wish to perform?",
                    "Select a number, or enter \"quit\" or \"exit\" to go back",
                    "\t0. Rename a meal",
                    "\t1. Edit dish probabilities for a meal"
                });
                var input = _consoleService.ReadLine();
                switch (input)
                {
                    case "quit":
                    case "exit":
                        return;
                    case "0":
                        RenameMeal();
                        return;
                    case "1":
                        EditMealWeights();
                        return;
                    default:
                        DisplayUnkownCommandMessage(input);
                        break;
                }
            }
        }

        private void EditMealWeights()
        {
            throw new NotImplementedException();
        }

        private void RenameMeal()
        {
            throw new NotImplementedException();
        }

        private void AddMeal()
        {
            _consoleService.WriteLines(new[] { "Let's add a meal! What should we call it?" });
            string mealName = null;
            while (string.IsNullOrEmpty(mealName))
            {
                mealName = _consoleService.ReadLine();
            }
            _mealManager.AddMeal(new Meal() { Name = mealName });
        }

        private void ListMeals()
        {
            var meals = _mealManager.ListMeals();
            var mealStrings = meals.Count == 0 
                ? new[] { "There are no meals. Use the \"add\" command to add a meal." }
                : meals.Select((x, i) => $"{i,3}.\t{x.Name}").ToArray();
            _consoleService.WriteLines(mealStrings);
        }

        private void DisplayMealEditorHelp()
        {
            _consoleService.WriteLines(new[]
            {
                "Commands:",
                "\t\"help\": display this help",
                "\t\"list\": list meals",
                "\t\taliases: \"ls\"",
                "\t\"add\": add a meal",
                "\t\"edit\": edit a meal",
                "\t\taliases: \"ed\"",
                "\t\"remove\": remove a meal",
                "\t\taliases: \"del\", \"rm\"",
                "\t\"quit\": go back to the main menu",
                "\t\taliases: \"exit\", \"q\""
            });
        }
    }
}
