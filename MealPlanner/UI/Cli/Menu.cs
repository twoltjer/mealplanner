namespace MealPlanner.UI.Cli
{
    /**
     * A command line interface menu class
     */
    public class Menu
    {
        private readonly IConsoleService _consoleService;
        private readonly IMealEditor _mealEditor;

        /**
         * Create a command line interface for the meal planner using an IConsoleService.
         * @param consoleService the console service used to get and write text to the console
         */
        public Menu(
            IConsoleService consoleService,
            IMealEditor mealEditor)
        {
            _consoleService = consoleService;
            _mealEditor = mealEditor;
        }

        /**
         * Runs the menu continuously until the user enters the quit command.
         */
        public void RunMenu()
        {
            while(true)
            {
                DisplayMenuWelcome();
                var input = _consoleService.ReadLine();
                switch (input.ToLower())
                {
                    case "help":
                        DisplayHelp();
                        break;
                    case "em":
                        _mealEditor.RunMealEditor();
                        break;
                    case "exit":
                    case "quit":
                    case "q":
                        DisplayGoodbye();
                        return;
                    default:
                        HandleUnknownInput(input);
                        break;
                }
            }
        }

        private void DisplayMenuWelcome()
        {
            _consoleService.WriteLines(new[] {
                "",
                "Welcome to Thomas' meal planner!",
                "For a list of commands, type \"help\"",
                "To quit, type \"quit\" or \"exit\""
            });
        }

        private void DisplayHelp()
        {
            _consoleService.WriteLines(new[] {
                "Commands:",
                "\t\"help\": display this help",
                "\t\"em\": edit meals",
                "\t\"quit\": quit the application",
                "\t\taliases: \"exit\", \"q\""
            });
        }

        private void DisplayGoodbye()
        {
            _consoleService.WriteLines(new[] {
                "Goodbye!"
            });
        }

        private void HandleUnknownInput(string input)
        {
            _consoleService.WriteLines(new[] {
                $"Unknown command: \"{input}\""
            });
            DisplayHelp();
        }
    }
}
