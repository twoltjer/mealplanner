﻿using MealPlanner.Definitions;
using System.Collections.Generic;

namespace MealPlanner.Managers.MealManager
{
    public class MealManager : IMealManager
    {
        public MealManager()
        {
            _meals = new List<Meal>();
        }
        private List<Meal> _meals;
        public IList<Meal> ListMeals()
        {
            return _meals;
        }

        public void AddMeal(Meal meal)
        {
            _meals.Add(meal);
        }

        public void RemoveMeal(int index)
        {
            _meals.RemoveAt(index);
        }

        public bool IsValidMealIndex(int index)
        {
            if (index < 0)
                return false;
            return index < _meals.Count;
        }
    }
}
