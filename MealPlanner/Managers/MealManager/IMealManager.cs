﻿using MealPlanner.Definitions;
using System.Collections.Generic;

namespace MealPlanner.Managers
{
    public interface IMealManager
    {
        public IList<Meal> ListMeals();
        public void AddMeal(Meal meal);
        public void RemoveMeal(int index);
        public bool IsValidMealIndex(int index);
    }
}
