﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace MealPlanner.Calendar.Dates
{
    /**
     * Represents a single date
     */
    public sealed class Date
    {
        /**
         * Create a date using a day of the month, a month number, and a four-digit year
         */
        public Date(int day, int month, int year)
        {
            Day = day;
            Month = month;
            Year = year;
        }

        /**
         * The day number (of the month) represented in this date
         */
        public int Day { get; }

        /**
         * The month number represented in this date
         */
        public int Month { get; }

        /**
         * The four digit year represented in this date
         */
        public int Year { get; }

        /**
         * Tests if another object is equivalent to this date
         * @param obj any object
         * @return if the other object is the same date as this one
         */
        public override bool Equals(object obj)
        {
            if (obj is Date other)
            {
                return other.Day == Day && other.Month == Month && other.Year == Year;
            }
            return base.Equals(obj);
        }

        /**
         * Creates a hash code based on the day, month, and year
         * @return 
         */
        public override int GetHashCode()
        {
            return HashCode.Combine(Day, Month, Year);
        }

        /**
         * Attempts to parse a string into a date.
         * The string must be in "<month>/<day>/<year>" format
         */
        public static bool TryParse(string dateString, out Date date)
        {
            if (Regex.IsMatch(dateString, @"^\d+/\d+/\d+$"))
            {
                var match = Regex.Match(dateString, @"^(?'month'\d+)\/(?'dayOfTheMonth'\d+)\/(?'year'\d+)$");
                int dayOfTheMonth = int.Parse(match.Groups.Values.Single(x => x.Name.Equals("dayOfTheMonth")).Value);
                int month = int.Parse(match.Groups.Values.Single(x => x.Name.Equals("month")).Value);
                int year = int.Parse(match.Groups.Values.Single(x => x.Name.Equals("year")).Value);
                if (year < 100)
                    year += 2000;
                date = new Date(dayOfTheMonth, month, year);
                return true;
            }
            date = default;
            return false;
        }
    }
}
