﻿using System;
using System.Collections.Generic;

namespace MealPlanner.Calendar.Dates
{
    /**
     * Turns two dates into a range of all dates between them, including the start and end date. 
     */
    public class DateRange
    {
        private readonly Date _start;
        private readonly Date _end;

        /**
         * Create a new date range. If the start date is after the end date, the range is empty.
         * @param start the date that the range should start with
         * @param end the date that the range should end with
         */
        public DateRange(Date start, Date end)
        {
            _start = start;
            _end = end;
        }

        /**
         * Gets all the dates between the start date and the end date (inclusively)
         */
        public List<Date> Dates
        {
            get
            {
                var dateList = new List<Date>();
                for (int year = _start.Year; year <= _end.Year; year++)
                {
                    var startMonth = year == _start.Year ? _start.Month : 1;
                    var endMonth = year == _end.Year ? _end.Month : 12;
                    for (int month = startMonth; month <= endMonth; month++)
                    {
                        var startDate = year == _start.Year && month == _start.Month
                            ? _start.Day
                            : 1;
                        var endDate = year == _end.Year && month == _end.Month
                            ? _end.Day
                            : DateTime.DaysInMonth(year, month);
                        for (int date = startDate; date <= endDate; date++)
                        {
                            dateList.Add(new Date(date, month, year));
                        }
                    }
                }
                return dateList;
            }
        }

    }
}
