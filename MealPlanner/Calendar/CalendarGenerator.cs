﻿using MealPlanner.Definitions;

namespace MealPlanner.Calendar
{
    class CalendarGenerator
    {
        private readonly Meal _meal;
        

        public CalendarGenerator(Meal meal)
        {
            _meal = meal;
        }

        public string Name =>
            $"{_meal.Name} Calendar";

        public string[] GenerateCalendarCsv()
        {
            return default;
        }
    }
}
