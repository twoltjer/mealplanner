﻿using System.Collections.Generic;

namespace MealPlanner.Definitions
{
    public class Meal
    {
        public string Name { get; set; }
        public IList<MealCourse> Courses { get; set; }
    }
}
