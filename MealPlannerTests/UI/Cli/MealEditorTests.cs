﻿using MealPlanner.Definitions;
using MealPlanner.Managers;
using MealPlanner.UI.Cli;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace MealPlannerTests.UI.Cli
{
    public class MealEditorTests
    {
        private Mock<IConsoleService> _consoleService;
        private Mock<IMealManager> _mealManager;
        private MockSequence _sequence;

        [Theory]
        [InlineData("quit")]
        [InlineData("exit")]
        [InlineData("q")]
        public void TestExitMealEditor(string command)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            AnticipateMealEditorWelcome();
            SendToConsole(command);

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Theory]
        [InlineData("list")]
        [InlineData("ls")]
        public void TestListMeals(string command)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();
            
            var mealList = new[]
            {
                new Meal() { Name = "Brunch" },
                new Meal() { Name = "Dinner" }
            };

            AnticipateMealEditorWelcome();
            SendToConsole(command);
            AnticipateMealManagerListCall(mealList);
            AnticipateOutputMealList(mealList);
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Theory]
        [InlineData("list")]
        [InlineData("ls")]
        public void TestListNoMeals(string command)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var mealList = Array.Empty<Meal>();

            AnticipateMealEditorWelcome();
            SendToConsole(command);
            AnticipateMealManagerListCall(mealList);
            AnticipateOutputEmptyMealList();
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Fact]
        public void TestMealEditorHelp()
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            AnticipateMealEditorWelcome();
            SendToConsole("help");
            AnticipateMealEditorHelp();
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Fact]
        public void TestAddMeal()
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var mealname = "Supper";

            AnticipateMealEditorWelcome();
            SendToConsole("add");
            AnticipateAddMealNameQuery();
            SendToConsole(mealname);
            AnticipateMealManagerAddMealCall(mealname);
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Theory]
        [InlineData("remove")]
        [InlineData("rm")]
        [InlineData("del")]
        public void TestRemoveMeal(string command)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var mealname = "Supper";

            AnticipateMealEditorWelcome();
            SendToConsole(command);
            AnticipateRemoveMealQuery();
            var testMeal = new Meal() { Name = mealname };
            AnticipateMealManagerListCall(new List<Meal>() { testMeal });
            AnticipateOutputMealList(new[] { testMeal });
            SendToConsole("0");
            AnticipateCheckIndexCall(0, true);
            AnticipateMealManagerRemoveCall(0);
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Theory]
        [InlineData("remove", "exit")]
        [InlineData("rm", "exit")]
        [InlineData("del", "exit")]
        [InlineData("remove", "quit")]
        [InlineData("rm", "quit")]
        [InlineData("del", "quit")]
        [InlineData("remove", "q")]
        [InlineData("rm", "q")]
        [InlineData("del", "q")]
        public void TestRemoveMealCancel(string removeCommand, string cancelCommand)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var mealname = "Supper";

            AnticipateMealEditorWelcome();
            SendToConsole(removeCommand);
            AnticipateRemoveMealQuery();
            var testMeal = new Meal() { Name = mealname };
            AnticipateMealManagerListCall(new List<Meal>() { testMeal });
            AnticipateOutputMealList(new[] { testMeal });
            SendToConsole(cancelCommand);
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Theory]
        [InlineData("remove")]
        [InlineData("rm")]
        [InlineData("del")]
        public void TestRemoveMealInvalidIndex(string command)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var mealname = "Supper";

            AnticipateMealEditorWelcome();
            SendToConsole(command);
            AnticipateRemoveMealQuery();
            var testMeal = new Meal() { Name = mealname };
            AnticipateMealManagerListCall(new List<Meal>() { testMeal });
            AnticipateOutputMealList(new[] { testMeal });
            SendToConsole("0");
            AnticipateCheckIndexCall(0, false);
            AnticipateInvalidIndexReprompt(0);
            SendToConsole("quit");
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Theory]
        [InlineData("remove")]
        [InlineData("rm")]
        [InlineData("del")]
        public void TestRemoveMealInvalidInput(string command)
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var mealname = "Supper";

            AnticipateMealEditorWelcome();
            SendToConsole(command);
            AnticipateRemoveMealQuery();
            var testMeal = new Meal() { Name = mealname };
            AnticipateMealManagerListCall(new List<Meal>() { testMeal });
            AnticipateOutputMealList(new[] { testMeal });
            SendToConsole("a");
            AnticipateInvalidInputReprompt("a");
            SendToConsole("quit");
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        [Fact]
        public void TestHandleUnknownCommand()
        {
            _consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            _mealManager = new Mock<IMealManager>(MockBehavior.Strict);
            _sequence = new MockSequence();

            var unknownCommand = "unknown";

            AnticipateMealEditorWelcome();
            SendToConsole(unknownCommand);
            AnticipateInvalidCommand(unknownCommand);
            AnticipateMealEditorWelcome();
            SendToConsole("quit");

            var subject = new MealEditor(_consoleService.Object, _mealManager.Object);
            subject.RunMealEditor();
        }

        private void AnticipateInvalidCommand(string unknownCommand)
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                $"Invalid command: \"{unknownCommand}\". Please try again."
            }));
        }

        private void AnticipateInvalidInputReprompt(string input)
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                $"Invalid input \"{input}\". Please try again."
            }));
        }

        private void AnticipateInvalidIndexReprompt(int index)
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                $"Invalid index \"{index}\". Please try again."
            }));
        }

        private void AnticipateCheckIndexCall(int index, bool response)
        {
            _mealManager.InSequence(_sequence).Setup(x => x.IsValidMealIndex(index)).Returns(response);
        }

        private void AnticipateMealManagerRemoveCall(int index)
        {
            _mealManager.InSequence(_sequence).Setup(x => x.RemoveMeal(index));
        }

        private void AnticipateRemoveMealQuery()
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                "Enter an index for a meal you wish to remove, or enter \"quit\" or \"exit\" to go back."
            }));
        }

        private void AnticipateMealManagerAddMealCall(string mealname)
        {
            _mealManager.InSequence(_sequence).Setup(x => x.AddMeal(It.Is<Meal>(x => x.Name.Equals(mealname))));
        }

        private void AnticipateAddMealNameQuery()
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                "Let's add a meal! What should we call it?"
            }));
        }

        private void AnticipateOutputEmptyMealList()
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                "There are no meals. Use the \"add\" command to add a meal."
            }));
        }

        

        private void AnticipateMealEditorHelp()
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                "Commands:",
                "\t\"help\": display this help",
                "\t\"list\": list meals",
                "\t\taliases: \"ls\"",
                "\t\"add\": add a meal",
                "\t\"edit\": edit a meal",
                "\t\taliases: \"ed\"",
                "\t\"remove\": remove a meal",
                "\t\taliases: \"del\", \"rm\"",
                "\t\"quit\": go back to the main menu",
                "\t\taliases: \"exit\", \"q\""
            }));
        }

        private void AnticipateOutputMealList(IList<Meal> mealList)
        {
            var mealStrings = mealList.Select((x, i) => $"{i,3}.\t{x.Name}").ToArray();
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(mealStrings));
        }

        private void AnticipateMealManagerListCall(IList<Meal> mealList)
        {
            _mealManager.InSequence(_sequence).Setup(x => x.ListMeals()).Returns(mealList);
        }

        private void SendToConsole(string command)
        {
            _consoleService.InSequence(_sequence).Setup(x => x.ReadLine()).Returns(command);
        }

        private void AnticipateMealEditorWelcome()
        {
            _consoleService.InSequence(_sequence).Setup(x => x.WriteLines(new[]
            {
                "",
                "Welcome to the meal editor!",
                "What would you like to do?",
                "For options, type \"help\""
            }));
        }
    }
}
