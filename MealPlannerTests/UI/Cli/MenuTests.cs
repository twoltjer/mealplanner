using Moq;
using Xunit;

namespace MealPlanner.UI.Cli
{
    public class MenuTests
    {
        [Theory]
        [InlineData("quit")]
        [InlineData("exit")]
        public void TestQuitCommand(string command)
        {
            var consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            var mockSequence = new MockSequence();

            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, command);
            AnticipateConsoleQuit(consoleService, mockSequence);

            var subject = new Menu(consoleService.Object, default);
            subject.RunMenu();
        }

        [Fact]
        public void TestHelpCommand()
        {
            var consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            var mockSequence = new MockSequence();

            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, "help");
            AnticipateHelpMessage(consoleService, mockSequence);
            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, "quit");
            AnticipateConsoleQuit(consoleService, mockSequence);

            var subject = new Menu(consoleService.Object, default);
            subject.RunMenu();
        }

        [Fact]
        public void TestEditMealsCommand()
        {
            var consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            var mealEditor = new Mock<IMealEditor>(MockBehavior.Strict);
            var mockSequence = new MockSequence();

            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, "em");
            AnticipateMealEditorRun(mealEditor, mockSequence);
            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, "quit");
            AnticipateConsoleQuit(consoleService, mockSequence);

            var subject = new Menu(consoleService.Object, mealEditor.Object);
            subject.RunMenu();
        }

        [Fact]
        public void TestUnknownCommand()
        {
            var unknownCommand = "abcd";
            var consoleService = new Mock<IConsoleService>(MockBehavior.Strict);
            var mockSequence = new MockSequence();

            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, unknownCommand);
            AnticipateUnknownCommandMessage(consoleService, mockSequence, unknownCommand);
            AnticipateHelpMessage(consoleService, mockSequence);
            AnticipateMenuWelcome(consoleService, mockSequence);
            SendConsoleCommand(consoleService, mockSequence, "quit");
            AnticipateConsoleQuit(consoleService, mockSequence);

            var subject = new Menu(consoleService.Object, default);
            subject.RunMenu();
        }

        private static void AnticipateMenuWelcome(Mock<IConsoleService> consoleService, MockSequence sequence)
        {
            consoleService.InSequence(sequence).Setup(x => x.WriteLines(new[]
            {
                "",
                "Welcome to Thomas' meal planner!",
                "For a list of commands, type \"help\"",
                "To quit, type \"quit\" or \"exit\""
            }));
        }

        private static void AnticipateConsoleQuit(Mock<IConsoleService> consoleService, MockSequence sequence)
        {
            consoleService.InSequence(sequence).Setup(x => x.WriteLines(new[]
            {
                "Goodbye!"
            }));
        }

        private static void SendConsoleCommand(Mock<IConsoleService> consoleService, MockSequence sequence, string command)
        {
            consoleService.InSequence(sequence).Setup(x => x.ReadLine()).Returns(command);
        }

        private static void AnticipateHelpMessage(Mock<IConsoleService> consoleService, MockSequence sequence)
        {
            consoleService.InSequence(sequence).Setup(x => x.WriteLines(new[]
            {
                "Commands:",
                "\t\"help\": display this help",
                "\t\"em\": edit meals",
                "\t\"quit\": quit the application",
                "\t\taliases: \"exit\", \"q\""
            }));
        }

        private static void AnticipateMealEditorRun(Mock<IMealEditor> mealEditor, MockSequence sequence)
        {
            mealEditor.InSequence(sequence).Setup(x => x.RunMealEditor());
        }

        private static void AnticipateUnknownCommandMessage(Mock<IConsoleService> consoleService, MockSequence sequence, string command)
        {
            consoleService.InSequence(sequence).Setup(x => x.WriteLines(new[]
            {
                $"Unknown command: \"{command}\""
            }));
        }
    }
}