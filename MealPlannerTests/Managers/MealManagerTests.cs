﻿using FluentAssertions;
using MealPlanner.Definitions;
using MealPlanner.Managers.MealManager;
using Xunit;

namespace MealPlannerTests.Managers
{
    public class MealManagerTests
    {
        [Fact]
        public void TestCreateEmptyMealManager()
        {
            var subject = new MealManager();
            subject.ListMeals().Should().BeEmpty();
        }

        [Fact]
        public void TestAddMeal()
        {
            var subject = new MealManager();
            var testMeal = new Meal() { Name = "Breakfast" };
            subject.AddMeal(testMeal);
            subject.ListMeals().Should().BeEquivalentTo(new[] { testMeal });
        }

        [Fact]
        public void TestAddMultipleMeals()
        {
            var subject = new MealManager();
            var testMeals = new[]
            {
                new Meal() { Name = "Breakfast" },
                new Meal() { Name = "Dinner" }
            };
            subject.AddMeal(testMeals[0]);
            subject.AddMeal(testMeals[1]);
            subject.ListMeals().Should().BeEquivalentTo(testMeals);
        }

        [Fact]
        public void TestRemoveMeal()
        {
            var subject = new MealManager();
            var testMeals = new[]
            {
                new Meal() { Name = "Breakfast" },
                new Meal() { Name = "Dinner" }
            };
            subject.AddMeal(testMeals[0]);
            subject.AddMeal(testMeals[1]);
            subject.RemoveMeal(0);
            subject.ListMeals().Should().BeEquivalentTo(new[] { testMeals[1] });
        }

        [Fact]
        public void TestRemoveAllMeals()
        {
            var subject = new MealManager();
            var testMeals = new[]
            {
                new Meal() { Name = "Breakfast" },
                new Meal() { Name = "Dinner" }
            };
            subject.AddMeal(testMeals[0]);
            subject.AddMeal(testMeals[1]);
            subject.RemoveMeal(0);
            subject.RemoveMeal(0);
            subject.ListMeals().Should().BeEmpty();
        }
    }
}
