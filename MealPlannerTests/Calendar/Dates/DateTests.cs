﻿using FluentAssertions;
using MealPlanner.Calendar.Dates;
using Xunit;

namespace MealPlannerTests.Calendar.Dates
{
    public class DateTests
    {
        [Fact]
        public void TestConstructor()
        {
            var date = new Date(1, 2, 3);
            date.Day.Should().Be(1);
            date.Month.Should().Be(2);
            date.Year.Should().Be(3);
        }

        [Fact]
        public void TestTryParseFail()
        {
            Date.TryParse("abc", out var date).Should().BeFalse();
            date.Should().BeNull();
        }

        [Fact]
        public void TestTryParseTwoDigitYear()
        {
            Date.TryParse("1/2/34", out var date).Should().BeTrue();
            date.Day.Should().Be(2);
            date.Month.Should().Be(1);
            date.Year.Should().Be(2034);
        }

        [Fact]
        public void TestTryParseFullYear()
        {
            Date.TryParse("1/2/1934", out var date).Should().BeTrue();
            date.Day.Should().Be(2);
            date.Month.Should().Be(1);
            date.Year.Should().Be(1934);
        }
    }
}
