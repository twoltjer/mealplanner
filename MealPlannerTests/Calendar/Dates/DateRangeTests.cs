﻿using FluentAssertions;
using MealPlanner.Calendar.Dates;
using Xunit;

namespace MealPlannerTests.Calendar.Dates
{
    public class DateRangeTests
    {
        [Fact]
        public void TestSimpleDateRange()
        {
            var start = new Date(1, 1, 2000);
            var end = new Date(3, 1, 2000);
            var subject = new DateRange(start, end);
            subject.Dates.Count.Should().Be(3);
            subject.Dates[0].Should().Be(start);
            subject.Dates[1].Should().Be(new Date(2, 1, 2000));
            subject.Dates[2].Should().Be(end);
        }

        [Fact]
        public void TestMonthSpanningDateRange()
        {
            var start = new Date(27, 2, 2019);
            var end = new Date(1, 3, 2019);
            var subject = new DateRange(start, end);
            subject.Dates.Count.Should().Be(3); // 27, 28, 1
        }

        [Fact]
        public void TestLeapYearSpanningDateRange()
        {
            var start = new Date(27, 2, 2020);
            var end = new Date(1, 3, 2020);
            var subject = new DateRange(start, end);
            subject.Dates.Count.Should().Be(4); // 27, 28, 29, 1
        }

        [Fact]
        public void TestYearSpanningDateRange()
        {
            var start = new Date(30, 12, 2020);
            var end = new Date(1, 1, 2021);
            var subject = new DateRange(start, end);
            subject.Dates.Count.Should().Be(3); // 30, 31, 1
        }

        [Fact]
        public void TestBackwardsDateRange()
        {
            var start = new Date(30, 12, 2020);
            var end = new Date(1, 1, 2020);
            var subject = new DateRange(start, end);
            subject.Dates.Should().BeEmpty();
        }
    }
}
