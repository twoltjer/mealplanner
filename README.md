# MealPlanner

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/twoltjer/mealplanner)](https://gitlab.com/twoltjer/mealplanner/builds)

This is a meal planner application that can run on any system with .NET Core 3.1. 

_Note: This project is currently in its initial development. Many planned core features have not yet been implemented._

## Building and running

### Dependencies

First, ensure that you have .NET Core 3.1 installed. The easiest way to do this is to run the following command:

```
dotnet --version
```

If your version number is not 3.1.### or newer (note: .NET Framework 4 is older than .NET Core 3), you probably need to install the latest .NET Core Runtime. Downloads can be found here: https://dotnet.microsoft.com/download/dotnet-core/current/runtime

### Download source code

The source for this project is available at https://gitlab.com/twoltjer/mealplanner. The code can be downloaded two ways. If you have `git` installed, you can clone the repository:

```
git clone https://gitlab.com/twoltjer/mealplanner.git
```

If you don't have git installed and don't wish to set it up, you may also download a .ZIP archive of the source: https://gitlab.com/twoltjer/mealplanner/-/archive/master/mealplanner-master.zip. After downloading the .ZIP file, extract it. 

### Building the MealPlanner application

To build the application, enter the project's root directory (the one with the `MealPlanner.sln` file) and run the following command:

```
dotnet build
```

The output should look something like this: 

```
$ dotnet build
Microsoft (R) Build Engine version 16.6.0+5ff7b0c9e for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  Restored C:\Users\Thomas\Documents\testmp\MealPlanner\MealPlanner.csproj (in 143 ms).
  Restored C:\Users\Thomas\Documents\testmp\MealPlannerTests\MealPlannerTests.csproj (in 609 ms).
  MealPlanner -> C:\Users\Thomas\Documents\testmp\MealPlanner\bin\Debug\netcoreapp3.1\MealPlanner.dll
  MealPlannerTests -> C:\Users\Thomas\Documents\testmp\MealPlannerTests\bin\Debug\netcoreapp3.1\MealPlannerTests.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:03.06
```

### Running the MealPlanner application

To run the application, run the following command from the project's root directory:

```
dotnet run --project MealPlanner/MealPlanner.csproj
```

## Development

### Development Dependencies

Development of the MealPlanner application requires the .NET Core SDK. Install it from here: https://dotnet.microsoft.com/download

### Testing

This project comes with a set of unit tests that should be used to aide development. They live in the MealPlannerTests directory, and use xUnit, Moq, and FluentAssertions to test components of the application. The tests in this project are automatically discovered. To run the test suite from the command line, use the following command:
```
dotnet test
```

Please ensure that all unit tests pass, and do some user testing before creating any pull requests. 